# color support
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls="ls --color=auto"
  alias dir="dir --color=auto"
  alias vdir="vdir --color=auto"

  alias grep="grep --color=auto"
  alias fgrep="fgrep --color=auto"
  alias egrep="egrep --color=auto"
fi

# ls
alias ll="ls -lF"
alias la="ls -lA"
alias l="ls -CF"

# other
alias my-ip="dig +short myip.opendns.com @resolver1.opendns.com"

