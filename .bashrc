# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar 2> /dev/null

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
  xterm|xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	  # We have color support; assume it's compliant with Ecma-48
	  # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	  # a case would tend to support setf rather than setaf.)
	  color_prompt=yes
  else
	  color_prompt=
  fi
fi

set-title() {
  # If this is an xterm set the title to user@host:dir
  case "$TERM" in
  xterm*|rxvt*)
    PS1="\[\e]0;\u@\h \w\a\]$PS1"
    ;;
  *)
    ;;
  esac
}

prompt() {
  PS1="\[\033[01;32m\]\u@\h\[\033[01;34m\] \w\[\033[00m\]$(git-colored-ps1-branch) $ "

  set-title
}

no-user-prompt() {
  PS1="\[\033[01;31m\]\h\[\033[01;34m\] \w\[\033[00m\]$(git-colored-ps1-branch) $ "

  set-title
}

no-color-prompt() {
  PS1="\u@\h \w$(git-ps1-branch) $ "

  set-title
}

# wrap colors in \[ \] or the terminal may not work as expected
if [ "$color_prompt" = yes ]; then
  if [[ ${EUID} == 0 ]] ; then
    PROMPT_COMMAND=no-user-prompt
  else
    PROMPT_COMMAND=prompt
  fi
else
  # this PS1 doesn't use a command that returns control sequences so it doesn't need to use PROMPT_COMMAND
  no-color-prompt
fi
unset color_prompt force_color_prompt

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Environment variables and functions.
if [ -f ~/.bash_env ]; then
  . ~/.bash_env
fi

# Extra paths
if [ -f ~/.bash_paths ]; then
  . ~/.bash_paths
fi

export VISUAL=vim
export EDITOR="$VISUAL"

