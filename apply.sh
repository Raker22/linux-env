#!/bin/bash
dir=`cd $(dirname "$0") && pwd`

exit_on_fail() {
  if [ $? -ne 0 ]; then
    exit $?
  fi
}

apply() {
  # make the corresponding file in $HOME source the file here
  for file in "$@"; do
    file_path="$HOME/$file"
    env_path="$dir/$file"

    # make sure the file exists
    touch "$file_path"

    # make sure the file isn't already sourcing this file
    if [ -z "`fgrep '. "'"$env_path"'"' "$file_path"`" ]; then

      cat >> "$file_path" << EOF
if [ -f "$env_path" ]; then
  . "$env_path"
fi

EOF

    fi
  done
}

# setup environment
apply .bash_aliases .bash_env .bash_paths .bashrc

. "$dir/.bashrc"

"$dir/configure_git.sh"

# add scripts
scripts=`ls "$dir/bin"`

"$dir/bin/mkpath" "$HOME/.bin"
exit_on_fail

for script in $scripts; do
  ln -sf "$dir/bin/$script" "$HOME/.bin/$script"
done

