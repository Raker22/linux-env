#!/bin/bash

git config --global alias.current "!git-current-branch"
git config --global alias.fetchone "!git-fetchone"
git config --global alias.fetchout "!git-fetchout"

git config --global alias.lg "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
git config --global alias.lg2 "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"

git config --global alias.co "checkout"
git config --global alias.cb "checkout -b"

git config --global alias.br "branch"

git config --global alias.cm "commit -m"
git config --global alias.amend "commit --amend --reset-author --no-edit"
git config --global alias.amend-edit "commit --amend --reset-author"

